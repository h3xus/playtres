// main.go
package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/cors"

	"srsgoapi/db"
	_ "srsgoapi/docs"
	"srsgoapi/handlers"
)

func main() {
	// os.Setenv("DB_HOST", "localhost")
	// os.Setenv("DB_PASSWORD", "haslo1")
	// os.Setenv("DB_USERNAME", "booker")
	// os.Setenv("DB_PORT", "3306")
	// os.Setenv("SITE_TITLE", "Test Site")
	// os.Setenv("DB_NAME", "bookings")

	if err := db.InitDB(); err != nil {
		log.Fatalf("Error initializing database: %v", err)
	}
	r := mux.NewRouter()

	// CORS handler
	c := cors.AllowAll()

	// Use CORS handler
	handler := c.Handler(r)

	r.HandleFunc("/bookings", handlers.GetBookingHandler).Methods("GET")
	r.HandleFunc("/bookings/create", handlers.CreateBookingHandler).Methods("POST")

	r.HandleFunc("/bookings/update", handlers.UpdateBookingHandler).Methods("POST")
	r.HandleFunc("/bookings/delete", handlers.RemoveBookingHandler).Methods("POST")

	r.HandleFunc("/bookings/user", handlers.GetBookingsByUserHandler).Methods("POST")
	r.HandleFunc("/bookings/week", handlers.GetBookingsForWeekHandler).Methods("POST")

	r.HandleFunc("/rooms/available", handlers.CheckAvailabilityOfRoomsHandler)
	r.HandleFunc("/rooms/availability", handlers.CheckAvailabilityHandler)

	r.HandleFunc("/localizations/all", handlers.GetLocalizationsHandler).Methods("GET")
	r.HandleFunc("/localizations/add", handlers.AddNewLocalizationHandler).Methods("POST")
	r.HandleFunc("/localizations/delete", handlers.RemoveLocalizationHandler).Methods("DELETE")
	r.HandleFunc("/localizations/update", handlers.UpdateLocalizationHandler).Methods("POST")
	// TODO: discontinue
	r.HandleFunc("/rooms/localizations", handlers.GetRoomsByLocalizationHandler).Methods("GET")

	r.HandleFunc("/rooms/all", handlers.GetRoomsHandler)
	r.HandleFunc("/rooms/add", handlers.AddNewRoomHandler).Methods("POST")
	r.HandleFunc("/rooms/update", handlers.UpdateRoomHandler).Methods("POST")
	r.HandleFunc("/rooms/delete", handlers.RemoveRoomHandler).Methods("DELETE")

	// Derpicate
	// r.HandleFunc("/rooms/permanent-equipment/{roomId}", handlers.GetPermanentEquipmentHandler).Methods("GET")
	// r.HandleFunc("/rooms/permanent-equipment", handlers.GetPermanentEquipmentHandler).Methods("GET")
	// MODS FOR ROOMS

	// equipments list
	r.HandleFunc("/equipment/all", handlers.GetEquipmentHandler).Methods("GET")
	r.HandleFunc("/equipment/types/all", handlers.GetEquipmentTypesHandler).Methods("GET")
	r.HandleFunc("/equipment/list", handlers.GetEquipmentsForRoomsHandler).Methods("POST")
	r.HandleFunc("/equipment/add", handlers.AddNewEquipmentHandler).Methods("POST")
	r.HandleFunc("/equipment/delete", handlers.RemoveEquipmentsDefinitionsHandler).Methods("DELETE")
	r.HandleFunc("/equipment/update", handlers.UpdateEquipmentsDefinitionsHandler).Methods("POST")

	// room settings
	r.HandleFunc("/users/all", handlers.GetUsersHandler).Methods("GET")
	r.HandleFunc("/roles/all", handlers.GetUserRolesHandler).Methods("GET")
	r.HandleFunc("/users/add", handlers.CreateUserHandler).Methods("POST")
	r.HandleFunc("/users/update", handlers.UpdateUserHandler).Methods("POST")
	r.HandleFunc("/users/delete", handlers.RemoveUserHandler).Methods("DELETE")

	r.HandleFunc("/settings/catering", handlers.GetCateringHandler).Methods("GET")
	r.HandleFunc("/settings/tables", handlers.GetTablesHandler).Methods("GET")

	// healthcheck
	r.HandleFunc("/health", handlers.HealthCheckHandler)
	// version
	r.HandleFunc("/version", handlers.VersionCheckHandler)

	// auth
	r.HandleFunc("/login", handlers.LoginHandler).Methods("POST")

	// log.Fatal(http.ListenAndServe(":7075", nil))
	log.Fatal(http.ListenAndServe(":7074", handler))
}
