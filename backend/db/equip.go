package db

import (
	"srsgoapi/models"
	"time"
)

func GetAllEquipments() ([]models.Equipments, error) {

	rows, err := db.Query(`SELECT * FROM equip_element`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var RoomEquipments []models.Equipments
	for rows.Next() {
		var RoomEquipment models.Equipments
		err := rows.Scan(
			&RoomEquipment.EquipID,
			&RoomEquipment.Name,
			&RoomEquipment.IsActive,
			&RoomEquipment.Type,
			&RoomEquipment.ForRoom,
			&RoomEquipment.Created,
			&RoomEquipment.CreatedBy,
			&RoomEquipment.Updated,
		)
		if err != nil {
			return nil, err
		}
		RoomEquipments = append(RoomEquipments, RoomEquipment)
	}

	return RoomEquipments, nil
}

func GetEquipmentForRoom(roomID int) ([]models.Equipments, error) {
	// Query to retrieve equipment for a specific room.
	rows, err := db.Query(`
        SELECT *
		FROM equip_element
		WHERE forRoom = ?
    `, roomID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var roomEquipments []models.Equipments
	for rows.Next() {
		var roomEquipment models.Equipments
		err := rows.Scan(
			&roomEquipment.EquipID,
			&roomEquipment.Name,
			&roomEquipment.IsActive,
			&roomEquipment.Type,
			&roomEquipment.ForRoom,
			&roomEquipment.Created,
			&roomEquipment.CreatedBy,
			&roomEquipment.Updated,
		)
		if err != nil {
			return nil, err
		}
		roomEquipments = append(roomEquipments, roomEquipment)
	}

	return roomEquipments, nil
}

func GetEquipmentTypes() ([]models.EquipmentsTypes, error) {
	// room_equipments_type
	rows, err := db.Query(`
        SELECT *
		FROM room_equipments_type
    `)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var roomEquipmentTypes []models.EquipmentsTypes
	for rows.Next() {
		var roomEquipmentType models.EquipmentsTypes
		err := rows.Scan(
			&roomEquipmentType.TypeID,
			&roomEquipmentType.Name,
			&roomEquipmentType.Codename,
		)
		if err != nil {
			return nil, err
		}
		roomEquipmentTypes = append(roomEquipmentTypes, roomEquipmentType)
	}

	return roomEquipmentTypes, nil
}

func AddNewEquipment(newEquipment models.NewEquipments) error {
	_, err := db.Exec("INSERT INTO  `equip_element` ( `name`, `isActive`, `type`, `forRoom`, `created`, `createdBy`, `updated`) VALUES (?, ?, ?, ?, ?, ?, ?)",
		newEquipment.Name,
		newEquipment.IsActive,
		newEquipment.Type,
		newEquipment.ForRoom,
		time.Now().Add(time.Hour*2), // Adjusting for Polish time zone which is UTC+2
		newEquipment.CreatedBy,
		time.Now().Add(time.Hour*2),
	)
	if err != nil {
		return err
	}

	return nil
}

func UpdateEquipment(updatedEquipment models.Equipments) error {
	_, err := db.Exec("UPDATE `equip_element` SET name=?, isActive=?, Updated=? WHERE id=?",
		updatedEquipment.Name,
		updatedEquipment.IsActive,
		updatedEquipment.Type,
		updatedEquipment.ForRoom,
		time.Now().Add(time.Hour*2),
		updatedEquipment.EquipID,
	)
	if err != nil {
		return err
	}

	return nil
}

func DeleteEquipment(EquipID models.Equipments) error {
	_, err := db.Exec("DELETE FROM `equip_element` WHERE id = ?", EquipID.EquipID)
	if err != nil {
		return err
	}
	return nil
}
