package db

import (
	"errors"
	"srsgoapi/models"
	"time"
)

// CreateBooking creates a new booking in the database and checks for time conflicts.
func CreateBooking(newBooking models.Booking) (int, error) {
	// Check for time conflicts before inserting the new booking
	if err := checkTimeConflicts(newBooking.RoomID, newBooking.BookingDateStart, newBooking.BookingDateEnd); err != nil {
		return 0, err
	}

	// If no conflicts, insert the new booking
	result, err := db.Exec(
		"INSERT INTO booked_rooms (title_meeting, meeting_descriptions, participants, user_id, room_id, status, "+
			"booking_date_start, booking_date_end, booking_params) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
		newBooking.TitleMeeting,
		newBooking.MeetingDescriptions,
		newBooking.Participants,
		newBooking.UserID,
		newBooking.RoomID,
		newBooking.Status,
		newBooking.BookingDateStart,
		newBooking.BookingDateEnd,
		newBooking.BookingParams,
	)
	if err != nil {
		return 0, err
	}

	// Get the ID of the new meeting
	meetingID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	// Update the booking ID
	newBooking.ID = int(meetingID)

	return newBooking.ID, nil
}

// UpdateBooking updates a booking in the database.
func UpdateBooking(updatedBooking models.Booking) error {
	_, err := db.Exec(
		"UPDATE booked_rooms SET title_meeting = ?, meeting_descriptions = ?, participants = ?, user_id = ?, room_id = ?, status = ?, "+
			"booking_date_start = ?, booking_date_end = ?, booking_params = ? WHERE id = ?",
		updatedBooking.TitleMeeting,
		updatedBooking.MeetingDescriptions,
		updatedBooking.Participants,
		updatedBooking.UserID,
		updatedBooking.RoomID,
		updatedBooking.Status,
		updatedBooking.BookingDateStart,
		updatedBooking.BookingDateEnd,
		updatedBooking.BookingParams,
		updatedBooking.ID,
	)
	if err != nil {
		return err
	}

	return nil
}

// SaveEquipmentForMeeting saves the equipment for a meeting in the database.
func SaveEquipmentForMeeting(equipment []models.SaveEquipmentForMeeting) error {
	for _, equip := range equipment {
		_, err := db.Exec(
			"INSERT INTO booked_room_equipment (booking_id, equipment_id, amount) VALUES (?, ?, ?)",
			equip.BookingID,
			equip.EquipmentID,
			equip.Amount,
		)
		if err != nil {
			return err
		}
	}
	return nil
}

// RemoveBooking removes a booking from the database.
func RemoveBooking(removeBooking models.Booking) error {
	_, err := db.Exec("DELETE FROM booked_rooms WHERE id = ?", removeBooking.ID)
	if err != nil {
		return err
	}

	return nil
}

func GetBookings() ([]models.Booking, error) {
	rows, err := db.Query("SELECT * FROM booked_rooms")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var bookings []models.Booking
	for rows.Next() {
		var booking models.Booking
		var bookingDateStart, bookingDateEnd string

		err := rows.Scan(
			&booking.ID,
			&booking.TitleMeeting,
			&booking.MeetingDescriptions,
			&booking.Participants,
			&booking.UserID,
			&booking.RoomID,
			&booking.Status,
			&bookingDateStart, // Scan as string
			&bookingDateEnd,   // Scan as string
			&booking.CreatedAt,
			&booking.BookingParams,
			&booking.Optionals,
		)
		if err != nil {
			return nil, err
		}

		// Convert string to time.Time
		booking.BookingDateStart, err = time.Parse("2006-01-02T15:04:05Z07:00", bookingDateStart)
		if err != nil {
			return nil, err
		}

		booking.BookingDateEnd, err = time.Parse("2006-01-02T15:04:05Z07:00", bookingDateEnd)
		if err != nil {
			return nil, err
		}

		bookings = append(bookings, booking)
	}

	return bookings, nil
}

// GetBookingsWithPagination retrieves a specific number of bookings from the database with pagination.
func GetBookingsWithPagination(itemNumber int) ([]models.Booking, error) {
	rows, err := db.Query("SELECT * FROM booked_rooms LIMIT ?", itemNumber)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var bookings []models.Booking
	for rows.Next() {
		var booking models.Booking
		var bookingDateStart, bookingDateEnd string

		err := rows.Scan(
			&booking.ID,
			&booking.TitleMeeting,
			&booking.MeetingDescriptions,
			&booking.Participants,
			&booking.UserID,
			&booking.RoomID,
			&booking.Status,
			&bookingDateStart, // Scan as string
			&bookingDateEnd,   // Scan as string
			&booking.CreatedAt,
			&booking.BookingParams,
			&booking.Optionals,
		)
		if err != nil {
			return nil, err
		}

		// Convert string to time.Time
		booking.BookingDateStart, err = time.Parse("2006-01-02T15:04:05Z07:00", bookingDateStart)
		if err != nil {
			return nil, err
		}

		booking.BookingDateEnd, err = time.Parse("2006-01-02T15:04:05Z07:00", bookingDateEnd)
		if err != nil {
			return nil, err
		}

		bookings = append(bookings, booking)
	}

	return bookings, nil
}

func RemoveBookingOrder(bookingData models.Booking) error {
	_, err := db.Exec("DELETE FROM booked_rooms WHERE id = ?", bookingData.ID)
	if err != nil {
		return err
	}
	return nil
}

func GetBookingsByUser(userID int) ([]models.Booking, error) {
	rows, err := db.Query("SELECT * FROM booked_rooms WHERE user_id = ?", userID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var bookings []models.Booking
	for rows.Next() {
		var booking models.Booking
		var bookingDateStart, bookingDateEnd string

		err := rows.Scan(
			&booking.ID,
			&booking.TitleMeeting,
			&booking.MeetingDescriptions,
			&booking.Participants,
			&booking.UserID,
			&booking.RoomID,
			&booking.Status,
			&bookingDateStart, // Scan as string
			&bookingDateEnd,   // Scan as string
			&booking.CreatedAt,
			&booking.BookingParams,
			&booking.Optionals,
		)
		if err != nil {
			return nil, err
		}

		// Convert string to time.Time
		booking.BookingDateStart, err = time.Parse("2006-01-02T15:04:05Z07:00", bookingDateStart)
		if err != nil {
			return nil, err
		}

		booking.BookingDateEnd, err = time.Parse("2006-01-02T15:04:05Z07:00", bookingDateEnd)
		if err != nil {
			return nil, err
		}

		bookings = append(bookings, booking)
	}

	return bookings, nil
}

// GetBookingsForWeek function retrieves all the bookings for a specific week.
// It takes the week number as an argument and returns a slice of Booking objects and an error.
// The error will be nil if the function successfully retrieves the bookings.
// If the week number is invalid or there is an error in the database query, the function will return an error.
// swagger:route GET /bookings/week/{weekNumber} bookings getBookingsForWeek
// Returns all the bookings for a specific week.
// Responses:
//   200: []Booking
//   400: error

func GetBookingsForWeek(weekNumber int) ([]models.Booking, error) {
	// Get the start and end of the week based on the week number
	year, week := time.Now().ISOWeek()
	if weekNumber != week {
		return nil, errors.New("Invalid week number")
	}
	startOfWeek := time.Date(year, 0, 0, 0, 0, 0, 0, time.UTC).AddDate(0, 0, (weekNumber-1)*7-int(time.Now().Weekday()))
	endOfWeek := startOfWeek.AddDate(0, 0, 7)

	rows, err := db.Query("SELECT * FROM booked_rooms WHERE booking_date_start >= ? AND booking_date_end <= ?", startOfWeek, endOfWeek)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var bookings []models.Booking
	for rows.Next() {
		var booking models.Booking
		var bookingDateStart, bookingDateEnd string

		err := rows.Scan(
			&booking.ID,
			&booking.TitleMeeting,
			&booking.MeetingDescriptions,
			&booking.Participants,
			&booking.UserID,
			&booking.RoomID,
			&booking.Status,
			&bookingDateStart, // Scan as string
			&bookingDateEnd,   // Scan as string
			&booking.CreatedAt,
			&booking.BookingParams,
			&booking.Optionals,
		)
		if err != nil {
			return nil, err
		}

		// Convert string to time.Time
		booking.BookingDateStart, err = time.Parse("2006-01-02T15:04:05Z07:00", bookingDateStart)
		if err != nil {
			return nil, err
		}

		booking.BookingDateEnd, err = time.Parse("2006-01-02T15:04:05Z07:00", bookingDateEnd)
		if err != nil {
			return nil, err
		}

		bookings = append(bookings, booking)
	}

	return bookings, nil
}

// GetMeetingsForPeriod function retrieves all the meetings for a specific period.
// It takes the start and end dates as arguments and returns a slice of Booking objects and an error.
// The error will be nil if the function successfully retrieves the meetings.
// If there is an error in the database query, the function will return an error.
// swagger:route GET /meetings/period/{start}/{end} meetings getMeetingsForPeriod
// Returns all the meetings for a specific period.
// Responses:
//   200: []Booking
//   400: error

func GetMeetingsForPeriod(start time.Time, end time.Time) ([]models.Booking, error) {
	rows, err := db.Query("SELECT * FROM booked_rooms WHERE booking_date_start >= ? AND booking_date_end <= ?", start, end)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var meetings []models.Booking
	for rows.Next() {
		var meeting models.Booking
		var meetingDateStart, meetingDateEnd string

		err := rows.Scan(
			&meeting.ID,
			&meeting.TitleMeeting,
			&meeting.MeetingDescriptions,
			&meeting.Participants,
			&meeting.UserID,
			&meeting.RoomID,
			&meeting.Status,
			&meetingDateStart, // Scan as string
			&meetingDateEnd,   // Scan as string
			&meeting.CreatedAt,
			&meeting.BookingParams,
			&meeting.Optionals,
		)
		if err != nil {
			return nil, err
		}

		// Convert string to time.Time
		meeting.BookingDateStart, err = time.Parse("2006-01-02T15:04:05Z07:00", meetingDateStart)
		if err != nil {
			return nil, err
		}

		meeting.BookingDateEnd, err = time.Parse("2006-01-02T15:04:05Z07:00", meetingDateEnd)
		if err != nil {
			return nil, err
		}

		meetings = append(meetings, meeting)
	}

	return meetings, nil
}
