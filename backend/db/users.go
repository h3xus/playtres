package db

import (
	"database/sql"
	"fmt"
	"srsgoapi/models"
)

func GetUsers() ([]models.User, error) {
	rows, err := db.Query("SELECT * FROM users")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var users []models.User
	for rows.Next() {
		var user models.User
		err := rows.Scan(
			&user.Id,
			&user.Name,
			&user.Email,
			&user.Role,
			&user.IsSuspended,
		)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}

	return users, nil
}

func GetUserRoles() ([]models.UserRole, error) {
	rows, err := db.Query("SELECT * FROM user_roles")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var roles []models.UserRole
	for rows.Next() {
		var role models.UserRole
		err := rows.Scan(
			&role.Id,
			&role.Name,
		)
		if err != nil {
			return nil, err
		}
		roles = append(roles, role)
	}

	return roles, nil
}

func CreateUser(newUser models.User) (int, error) {
	// Check if the user already exists based on email
	var existingUserID int
	err := db.QueryRow("SELECT id FROM users WHERE email = ?", newUser.Email).Scan(&existingUserID)
	if err == nil {
		return 0, fmt.Errorf("user with email %s already exists", newUser.Email)
	}
	if err != sql.ErrNoRows {
		return 0, err
	}

	// If no existing user, proceed to insert the new user
	result, err := db.Exec("INSERT INTO users (name, email, role, is_suspended) VALUES (?, ?, ?, ?)", newUser.Name, newUser.Email, newUser.Role, newUser.IsSuspended)
	if err != nil {
		return 0, err
	}

	userID, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(userID), nil
}

func UpdateUser(updatedUser models.User) error {
	_, err := db.Exec("UPDATE users SET name = ?, email = ?, role = ?, is_suspended = ? WHERE id = ?", updatedUser.Name, updatedUser.Email, updatedUser.Role, updatedUser.IsSuspended, updatedUser.Id)
	if err != nil {
		return err
	}
	return nil
}

func RemoveUser(removeUser models.User) error {
	_, err := db.Exec("DELETE FROM users WHERE id = ?", removeUser.Id)
	if err != nil {
		return err
	}
	return nil
}
