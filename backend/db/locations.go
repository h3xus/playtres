package db

import (
	"fmt"
	"srsgoapi/models"
)

// AddNewLocalization adds a new localization to the database.
func AddNewLocalization(newLocalization models.Localization) error {
	_, err := db.Exec(
		"INSERT INTO cr_localization (name) VALUES (?)",
		newLocalization.Name,
	)
	if err != nil {
		return err
	}

	return nil
}

// RemoveLocalization removes a localization from the database.
func RemoveLocalization(removeLocalization models.Localization) error {
	result, err := db.Exec(
		"DELETE FROM cr_localization WHERE id = ?",
		removeLocalization.Loc_id,
	)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rowsAffected == 0 {
		return fmt.Errorf("no localization found with id %d", removeLocalization.Loc_id)
	}

	return nil
}

func GetLocations() ([]models.Localization, error) {
	rows, err := db.Query("SELECT * FROM cr_localization")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var localizations []models.Localization
	for rows.Next() {
		var localization models.Localization
		err := rows.Scan(
			&localization.Loc_id,
			&localization.Name,
		)
		if err != nil {
			return nil, err
		}
		localizations = append(localizations, localization)
	}

	return localizations, nil
}

// UpdateLocalization updates an existing localization in the database.
func UpdateLocalization(updateLocalization models.Localization) error {
	_, err := db.Exec(
		"UPDATE cr_localization SET name = ? WHERE id = ?",
		updateLocalization.Name,
		updateLocalization.Loc_id,
	)
	if err != nil {
		return err
	}

	return nil
}
