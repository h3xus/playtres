package db

import (
	"srsgoapi/models"
	"time"
)

func AddNewRoom(newRoom models.ConferenceRoom) error {
	_, err := db.Exec("INSERT INTO  `conference_room` (`name`, `group`, `room_description`, `floor`, `building`, `delegation`, `localization`, `room_priority`,  `max_capacity`, `availability`, `session`, `catering`, `created`, `created_by`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
		newRoom.Name,
		newRoom.Group,
		newRoom.RoomDescription,
		newRoom.Floor,
		newRoom.Building,
		newRoom.Delegation,
		newRoom.Localization,
		newRoom.RoomPriority,
		newRoom.MaxCapacity,
		newRoom.Availability,
		newRoom.Session,
		newRoom.Catering,
		time.Now().Add(time.Hour*2),
		newRoom.CreatedBy,
	)
	if err != nil {
		return err
	}
	//

	return nil
}

func UpdateRoom(updatedRoom models.ConferenceRoom) error {
	_, err := db.Exec("UPDATE `conference_room` SET name=?, `group`=?, room_description=?, floor=?, building=?, delegation=?, localization=?, room_priority=?, max_capacity=?, availability=?, session=?, catering=?, updated=?, updated_by=? WHERE id=?",
		updatedRoom.Name,
		updatedRoom.Group,
		updatedRoom.RoomDescription,
		updatedRoom.Floor,
		updatedRoom.Building,
		updatedRoom.Delegation,
		updatedRoom.Localization,
		updatedRoom.RoomPriority,
		updatedRoom.MaxCapacity,
		updatedRoom.Availability,
		updatedRoom.Session,
		updatedRoom.Catering,
		time.Now().Add(time.Hour*2),
		updatedRoom.UpdatedBy,
		updatedRoom.RoomID,
	)
	if err != nil {
		return err
	}

	return nil
}

func RemoveRoom(removeRoom models.ConferenceRoom) error {
	_, err := db.Exec("DELETE FROM `conference_room` WHERE id=?", removeRoom.RoomID)
	if err != nil {
		return err
	}
	return nil
}

func GetRooms() ([]models.ConferenceRoom, error) {
	rows, err := db.Query("SELECT id, name, room_description, max_capacity, localization, floor, room_priority, catering, created, created_by FROM `conference_room`")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var ConferenceRooms []models.ConferenceRoom
	for rows.Next() {
		var ConferenceRoom models.ConferenceRoom
		err := rows.Scan(
			&ConferenceRoom.RoomID,
			&ConferenceRoom.Name,
			&ConferenceRoom.RoomDescription,
			&ConferenceRoom.MaxCapacity,
			&ConferenceRoom.Localization,
			&ConferenceRoom.Floor,
			&ConferenceRoom.RoomPriority,
			&ConferenceRoom.Catering,
			&ConferenceRoom.Created,
			&ConferenceRoom.CreatedBy,
		)
		if err != nil {
			return nil, err
		}
		ConferenceRooms = append(ConferenceRooms, ConferenceRoom)
	}

	return ConferenceRooms, nil
}

// GetRoomsByLocalization retrieves conference rooms based on localization.
func GetRoomsByLocalization(localizationID int) ([]models.ConferenceRoom, error) {
	rows, err := db.Query("SELECT * FROM conference_room WHERE localization = ?", localizationID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var conferenceRooms []models.ConferenceRoom
	for rows.Next() {
		var room models.ConferenceRoom
		err := rows.Scan(
			&room.RoomID,
			&room.Name,
			&room.RoomDescription,
			&room.Delegation,
			&room.Localization,
			&room.RoomPriority,
			&room.Created,
			&room.CreatedBy,
			&room.Updated,
			&room.UpdatedBy,
		)
		if err != nil {
			return nil, err
		}
		conferenceRooms = append(conferenceRooms, room)
	}

	return conferenceRooms, nil
}
