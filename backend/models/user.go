package models

type User struct {
	Id          int    `json:"id"`
	Name        string `json:"name"`
	Email       string `json:"email"`
	Password    string `json:"password"`
	Role        int    `json:"role"`
	IsSuspended int    `json:"is_suspended"`
}

type UserRole struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}
