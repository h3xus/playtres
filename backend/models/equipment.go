// models/booking.go
package models

import "time"

type Equipments struct {
	EquipID   int       `json:"id"`
	Name      string    `json:"name"`
	IsActive  int       `json:"isActive"`
	Type      int       `json:"type"`
	ForRoom   int       `json:"forRoom"`
	Created   time.Time `json:"created"`
	CreatedBy int       `json:"createdBy"`
	Updated   time.Time `json:"updated"`
}

type NewEquipments struct {
	EquipID   int       `json:"id"`
	Name      string    `json:"name"`
	IsActive  int       `json:"isActive"`
	Type      int       `json:"type"`
	ForRoom   int       `json:"forRoom"`
	Created   time.Time `json:"created"`
	CreatedBy int       `json:"createdBy"`
	Updated   time.Time `json:"updated"`
}

type EquipmentsTypes struct {
	TypeID   int    `json:"id"`
	Name     string `json:"name"`
	Codename string `json:"codename"`
}
