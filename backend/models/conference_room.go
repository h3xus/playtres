// models/conference_room.go
package models

import "time"

type ConferenceRoom struct {
	RoomID          int       `json:"id"`
	Name            string    `json:"name"`
	Group           int       `json:"group"`
	RoomDescription string    `json:"room_description"`
	Building        int       `json:"building"`
	Floor           int       `json:"floor"`
	Delegation      int       `json:"delegation"`
	Localization    int       `json:"localization"`
	RoomPriority    int       `json:"room_priority"`
	MaxCapacity     int       `json:"max_capacity"`
	Availability    int       `json:"availability"`
	Session         int       `json:"session"`
	Catering        int       `json:"catering"`
	Created         time.Time `json:"created"`
	CreatedBy       int       `json:"created_by"`
	Updated         time.Time `json:"updated"`
	UpdatedBy       int       `json:"updated_by"`
}
