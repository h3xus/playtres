// models/booking.go
package models

import "time"

type Booking struct {
	ID                  int       `json:"id"`
	TitleMeeting        string    `json:"title_meeting"`
	MeetingDescriptions string    `json:"meeting_descriptions"`
	Participants        int       `json:"participants"`
	UserID              int       `json:"user_id"`
	RoomID              int       `json:"room_id"`
	Status              string    `json:"status"`
	BookingDateStart    time.Time `json:"booking_date_start" time_format:"2006-01-02T15:04:05Z07:00" time_utc:"true"`
	BookingDateEnd      time.Time `json:"booking_date_end" time_format:"2006-01-02T15:04:05Z07:00" time_utc:"true"`
	CreatedAt           time.Time `json:"created_at" time_format:"2006-01-02T15:04:05Z07:00" time_utc:"true"`
	BookingParams       int       `json:"booking_params"`
	Optionals           int       `json:"optionals"`
}
