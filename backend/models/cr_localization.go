// models/cr_localization.go
package models

type Localization struct {
	Loc_id int    `json:"id"`
	Name   string `json:"name"`
}
