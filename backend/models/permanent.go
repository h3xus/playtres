// models/booking.go
package models

// depricated
type PermanentEquipment struct {
	ID                   int `json:"id"`
	RoomID               int `json:"participants"`
	Screen               int `json:"screen"`
	Projector            int `json:"projector"`
	NumbersOfMicrophones int `json:"numbersofmicrophones"`
	MicrophoneStand      int `json:"microphonestand"`
	SoundConnection      int `json:"soundconnection"`
	Rostrum              int `json:"rostrum"`
	InternetWiFi         int `json:"internetwifi"`
	InternetEth          int `json:"interneteth"`
	Miejska              int `json:"miejska"`
	// TitleMeeting     string    `json:"title_meeting"`
	// Participants     int       `json:"participants"`
	// UserID           int       `json:"user_id"`
	// RoomID           int       `json:"room_id"`
	// Status           string    `json:"status"`
	// BookingDateStart time.Time `json:"booking_date_start"`
	// BookingDateEnd   time.Time `json:"booking_date_end"`
	// CreatedAt        time.Time `json:"created_at"`
	// BookingParams    int       `json:"booking_params"`
	// Optionals        int       `json:"optionals"`
}
