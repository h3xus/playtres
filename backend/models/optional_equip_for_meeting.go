package models

import "time"

type SaveEquipmentForMeeting struct {
	BookingID   int       `json:"booking_id"`
	EquipmentID int       `json:"equipment_id"`
	Amount      int       `json:"amount"`
	CreatedBy   int       `json:"created_by_user"`
	Created     time.Time `json:"created"`
	Updated     time.Time `json:"updated"`
}
