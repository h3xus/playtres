// models/settings.go
package models

type TableSettings struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type CateringSettings struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}
