// models/booking.go
package models

import "time"

type EquipmentsForRoom struct {
	RoomID         int       `json:"room_id"`
	EquipmentID    int       `json:"equip_id"`
	Room_name      string    `json:"conference_room_name"`
	Equipment_name string    `json:"equipment_name"`
	Amount         int       `json:"amount"`
	Type           int       `json:"type"`
	CreatedBy      int       `json:"created_by_user"`
	Created        time.Time `json:"created"`
	Updated        time.Time `json:"updated"`
}
