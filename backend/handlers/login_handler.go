package handlers

import (
	"encoding/json"
	"errors"
	"net/http"
	"srsgoapi/models"
)

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	// Decode the JSON body
	var credentials struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	err := json.NewDecoder(r.Body).Decode(&credentials)
	if err != nil {
		http.Error(w, "Invalid request body", http.StatusBadRequest)
		return
	}

	// Validate credentials
	if credentials.Username == "" || credentials.Password == "" {
		http.Error(w, "Username and password are required", http.StatusBadRequest)
		return
	}

	// Authenticate user
	user, err := authenticate(credentials.Username, credentials.Password)
	if err != nil {
		http.Error(w, "Invalid username or password", http.StatusUnauthorized)
		return
	}

	// Generate JWT token
	token, err := generateJWT(user)
	if err != nil {
		http.Error(w, "Failed to generate token", http.StatusInternalServerError)
		return
	}

	// Return the token
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"token": token})
}

func authenticate(username, password string) (models.User, error) {
	// This should check the credentials against a user database or service
	// Dummy check for example purposes
	if username == "admin" && password == "password" {
		return models.User{Name: username}, nil
	}
	return models.User{}, errors.New("invalid credentials")
}

func generateJWT(user models.User) (string, error) {
	// This should generate a JWT token for authenticated users
	// Dummy token for example purposes
	return "dummytoken123", nil
}
