// handlers/booking_handler.go
package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"srsgoapi/db"
	"srsgoapi/models"
	"strconv"
	"time"
)

// GetBookingHandler retrieves all bookings from the database and sends them as JSON.
func GetBookingHandler(w http.ResponseWriter, r *http.Request) {
	// Implement logic to retrieve bookings from the database
	bookings, err := db.GetBookings()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(bookings)
}

// GetBookingsForWeekHandler function is a handler for the GetBookingsForWeek function.
// It takes a HTTP response writer and a HTTP request as arguments.
// It reads the week number from the request body, retrieves the bookings for the week, and returns them as JSON.
// If there is an error in the database query or the JSON encoding, it returns an error.
// swagger:route POST /bookings/week bookings GetBookingsForWeekHandler
// Returns the bookings for the week
// responses:
//   200: bookingsResponse
//   400: errorResponse
//   500: errorResponse

func GetBookingsForWeekHandler(w http.ResponseWriter, r *http.Request) {
	// Parse JSON from the request body
	var weekNumber models.DataModels
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&weekNumber)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Retrieve bookings for the week from the database
	bookings, err := db.GetBookingsForWeek(weekNumber.WeekNumber)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return the bookings as JSON
	json.NewEncoder(w).Encode(bookings)
}

// GetMeetingsForNextTwoWeeksHandler function retrieves all the meetings for the next two weeks.
// It calculates the date 3 days before today and the date 2 weeks from today.
// It then retrieves the meetings for this period and returns them as JSON.
// If there is an error in the database query or the JSON encoding, it returns an error.
// swagger:route GET /meetings/nexttwoweeks meetings GetMeetingsForNextTwoWeeksHandler
// Returns the meetings for the next two weeks
// responses:
//   200: meetingsResponse
//   400: errorResponse
//   500: errorResponse

func GetMeetingsForNextTwoWeeksHandler(w http.ResponseWriter, r *http.Request) {
	// Calculate the date 3 days before today
	threeDaysBefore := time.Now().AddDate(0, 0, -3)

	// Calculate the date 2 weeks from today
	twoWeeksFromNow := time.Now().AddDate(0, 0, 14)

	// Retrieve meetings for the next two weeks from the database
	meetings, err := db.GetMeetingsForPeriod(threeDaysBefore, twoWeeksFromNow)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return the meetings as JSON
	json.NewEncoder(w).Encode(meetings)
}

// // Depricated
// // GetPermanentEquipmentHandler retrieves permanent equipment by room ID and sends it as JSON.
// func GetPermanentEquipmentHandler(w http.ResponseWriter, r *http.Request) {
// 	// Extract the room ID from the request URL
// 	vars := mux.Vars(r)
// 	roomIDStr := vars["roomId"]

// 	// Convert the room ID string to an integer
// 	roomID, err := strconv.Atoi(roomIDStr)
// 	if err != nil {
// 		http.Error(w, "Invalid room ID", http.StatusBadRequest)
// 		return
// 	}

// 	// Retrieve permanent equipment by room ID from the database
// 	permanentEquipment, err := db.GetPermanentEquipmentByRoomID(roomID)
// 	if err != nil {
// 		http.Error(w, err.Error(), http.StatusInternalServerError)
// 		return
// 	}

// 	// Send the permanent equipment as JSON response
// 	json.NewEncoder(w).Encode(permanentEquipment)
// }

// RemoveBookingHandler removes a booking from the database.
func RemoveBookingHandler(w http.ResponseWriter, r *http.Request) {
	// Parse JSON from the request body
	var removeBooking models.Booking
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&removeBooking)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Remove the booking from the database
	err = db.RemoveBookingOrder(removeBooking)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusOK)
	// w.Write([]byte("Booking removed successfully"))
	w.Write([]byte("Rezerwacja usunięta pomyślnie"))
}

// UpdateBookingHandler updates a booking in the database.
func UpdateBookingHandler(w http.ResponseWriter, r *http.Request) {
	// Parse JSON from the request body
	var updatedBooking models.Booking
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&updatedBooking)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Update the booking in the database
	err = db.UpdateBooking(updatedBooking)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusOK)
	// w.Write([]byte("Booking updated successfully"))
	w.Write([]byte("Rezerwacja zaktualizowana pomyślnie"))
}

// GetUsersHandler retrieves all settings from the database and sends them as JSON.
func GetTablesHandler(w http.ResponseWriter, r *http.Request) {
	// Implement logic to retrieve bookings from the database
	tables, err := db.GetTableSettings()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(tables)
}

// GetRooms retrieves all bookings from the database and sends them as JSON.
func GetRoomsHandler(w http.ResponseWriter, r *http.Request) {
	// Implement logic to retrieve bookings from the database
	ConferenceRooms, err := db.GetRooms()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(ConferenceRooms)
}

func CreateBookingHandler(w http.ResponseWriter, r *http.Request) {

	// Check if the request is a preflight request (OPTIONS method)
	if r.Method == http.MethodOptions {
		w.WriteHeader(http.StatusOK)
		return
	}

	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// Parse JSON from the request body
	var newBooking models.Booking
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&newBooking)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()
	// Create the booking in the database
	bookingID, err := db.CreateBooking(newBooking)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusCreated)
	// w.Write([]byte(fmt.Sprintf("Booking created successfully, ID: %d", bookingID)))
	w.Write([]byte(fmt.Sprintf("Rezerwacja dodana pomyślnie, ID: %d", bookingID)))
}

func SaveEquipmentForMeetingHandler(w http.ResponseWriter, r *http.Request) {
	// Check if the request is a preflight request (OPTIONS method)
	if r.Method == http.MethodOptions {
		w.WriteHeader(http.StatusOK)
		return
	}

	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// Parse JSON from the request body
	var equipment []models.SaveEquipmentForMeeting
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&equipment)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Save the equipment for the meeting in the database
	err = db.SaveEquipmentForMeeting(equipment)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusCreated)
	// w.Write([]byte("Equipment saved for meeting successfully"))
	w.Write([]byte("Sprzęt zapisany pomyślnie na spotkanie"))
}

func CheckAvailabilityHandler(w http.ResponseWriter, r *http.Request) {
	// Parse request parameters
	startStr := r.URL.Query().Get("start")
	start, err := time.Parse("2006-01-02T15:04:05Z", startStr)
	if err != nil {
		fmt.Println(startStr)
		http.Error(w, "Invalid start date", http.StatusBadRequest)
		return
	}

	endStr := r.URL.Query().Get("start")
	end, err := time.Parse("2006-01-02T15:04:05Z", endStr)
	if err != nil {
		fmt.Println(endStr)
		http.Error(w, "Invalid start date", http.StatusBadRequest)
		return
	}

	participants, err := strconv.Atoi(r.URL.Query().Get("participants"))
	if err != nil {
		http.Error(w, "Invalid participants value", http.StatusBadRequest)
		return
	}

	localization := r.URL.Query().Get("localization")

	// Get available rooms
	availableRooms, err := db.GetAvailableRooms(start, end, participants, localization)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Error getting available rooms:", err)
		return
	}

	// Convert the result to JSON without nulls
	jsonResult, err := json.Marshal(availableRooms)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Error marshaling JSON:", err)
		return
	}

	// Set Content-Type header
	w.Header().Set("Content-Type", "application/json")

	// Write the JSON response
	w.Write(jsonResult)
}

func CheckAvailabilityOfRoomsHandler(w http.ResponseWriter, r *http.Request) {
	// Parse request parameters
	startStr := r.URL.Query().Get("start")
	start, err := time.Parse("2006-01-02T15:04:05Z", startStr)
	if err != nil {
		log.Println(startStr)
		http.Error(w, "Invalid start date", http.StatusBadRequest)
		return
	}

	endStr := r.URL.Query().Get("start")
	end, err := time.Parse("2006-01-02T15:04:05Z", endStr)
	if err != nil {
		log.Println(endStr)
		http.Error(w, "Invalid start date", http.StatusBadRequest)
		return
	}

	participants, err := strconv.Atoi(r.URL.Query().Get("participants"))
	if err != nil {
		http.Error(w, "Invalid participants value", http.StatusBadRequest)
		return
	}

	localization := r.URL.Query().Get("localization")

	// Get available rooms
	availableRooms, err := db.GetAvailablity(start, end, participants, localization)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Error getting available rooms:", err)
		return
	}

	// Convert the result to JSON without nulls
	jsonResult, err := json.Marshal(availableRooms)
	if err != nil {
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		log.Println("Error marshaling JSON:", err)
		return
	}

	// Set Content-Type header
	w.Header().Set("Content-Type", "application/json")

	// Write the JSON response
	w.Write(jsonResult)
}

func GetBookingsByUserHandler(w http.ResponseWriter, r *http.Request) {
	// Parse JSON from the request body
	var user models.Booking
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Retrieve bookings for the user from the database
	bookings, err := db.GetBookingsByUser(user.UserID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return the bookings as JSON
	json.NewEncoder(w).Encode(bookings)
}
