package handlers

import (
	"encoding/json"
	"net/http"
	"srsgoapi/db"
)

// GetUsersHandler retrieves all settings from the database and sends them as JSON.
func GetCateringHandler(w http.ResponseWriter, r *http.Request) {
	// Implement logic to retrieve bookings from the database
	catering, err := db.GetCatering()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(catering)
}
