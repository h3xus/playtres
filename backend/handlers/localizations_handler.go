// handlers/booking_handler.go
package handlers

import (
	"encoding/json"
	"net/http"
	"srsgoapi/db"
	"srsgoapi/models"
	"strconv"
)

// AddNewLocalization

// GetLocalizationsHandler retrieves all localizations from the database and sends them as JSON.
func GetLocalizationsHandler(w http.ResponseWriter, r *http.Request) {
	// Implement logic to retrieve bookings from the database
	localizations, err := db.GetLocations()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(localizations)
}

// AddNewLocalizationHandler adds a new localization to the database.
func AddNewLocalizationHandler(w http.ResponseWriter, r *http.Request) {
	// Parse JSON from the request body
	var newLocalization models.Localization
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&newLocalization)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Add the localization to the database
	err = db.AddNewLocalization(newLocalization)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusCreated)
	// w.Write([]byte("Localization added successfully"))
	w.Write([]byte("Lokalizacja dodana pomyślnie"))
}

// RemoveLocalizationHandler removes a localization from the database.
func RemoveLocalizationHandler(w http.ResponseWriter, r *http.Request) {
	// Parse JSON from the request body
	var removeLocalization models.Localization
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&removeLocalization)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Remove the localization from the database
	err = db.RemoveLocalization(removeLocalization)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusOK)
	// w.Write([]byte("Localization removed successfully"))
	w.Write([]byte("Lokalizacja usunięta pomyślnie"))
}

func GetRoomsByLocalizationHandler(w http.ResponseWriter, r *http.Request) {
	// Parse the localization ID parameter from the query string
	locIDStr := r.URL.Query().Get("localization_id")
	if locIDStr == "" {
		http.Error(w, "Localization ID parameter is required", http.StatusBadRequest)
		return
	}

	// Convert the localization ID string to an integer
	localizationID, err := strconv.Atoi(locIDStr)
	if err != nil {
		http.Error(w, "Invalid localization ID", http.StatusBadRequest)
		return
	}

	// Retrieve conference rooms based on localization from the database
	conferenceRooms, err := db.GetRoomsByLocalization(localizationID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return the conference rooms as JSON
	json.NewEncoder(w).Encode(conferenceRooms)
}

func UpdateLocalizationHandler(w http.ResponseWriter, r *http.Request) {
	// Parse JSON from the request body
	var updateLocalization models.Localization
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&updateLocalization)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Update the localization in the database
	err = db.UpdateLocalization(updateLocalization)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message
	w.WriteHeader(http.StatusOK)
	// w.Write([]byte("Localization updated successfully"))
	w.Write([]byte("Lokalizacja zaktualizowana pomyślnie"))
}
