package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"srsgoapi/db"
	"srsgoapi/models"
)

// GetUsersHandler retrieves all settings from the database and sends them as JSON.
func GetUsersHandler(w http.ResponseWriter, r *http.Request) {
	// Implement logic to retrieve bookings from the database
	users, err := db.GetUsers()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(users)
}

// GetUserRolesHandler retrieves all user roles from the database and sends them as JSON.
func GetUserRolesHandler(w http.ResponseWriter, r *http.Request) {
	// Implement logic to retrieve user roles from the database
	roles, err := db.GetUserRoles()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(roles)
}

// CreateUserHandler creates a new user in the database.
func CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	// Check if the request is a preflight request (OPTIONS method)
	if r.Method == http.MethodOptions {
		w.WriteHeader(http.StatusOK)
		return
	}

	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// Parse JSON from the request body
	var newUser models.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&newUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Create the user in the database
	userID, err := db.CreateUser(newUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusCreated)
	// w.Write([]byte(fmt.Sprintf("User created successfully, ID: %d", userID)))
	w.Write([]byte(fmt.Sprintf("Użytkownik dodany pomyślnie, ID: %d", userID)))
}

// UpdateUserHandler updates a user in the database.
func UpdateUserHandler(w http.ResponseWriter, r *http.Request) {
	// Check if the request is a preflight request (OPTIONS method)
	if r.Method == http.MethodOptions {
		w.WriteHeader(http.StatusOK)
		return
	}

	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// Parse JSON from the request body
	var updatedUser models.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&updatedUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Update the user in the database
	err = db.UpdateUser(updatedUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusOK)
	// w.Write([]byte("User updated successfully"))
	w.Write([]byte("Użytkownik zaktualizowany pomyślnie"))
}

// RemoveUserHandler removes a user from the database.
func RemoveUserHandler(w http.ResponseWriter, r *http.Request) {
	// Parse JSON from the request body
	var removeUser models.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&removeUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Remove the user from the database
	err = db.RemoveUser(removeUser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusOK)
	// w.Write([]byte("User removed successfully"))
	w.Write([]byte("Użytkownik usunięty pomyślnie"))
}
