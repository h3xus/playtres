package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"srsgoapi/db"
	"srsgoapi/models"
)

func GetEquipmentsForRoomsHandler(w http.ResponseWriter, r *http.Request) {
	// Parse room ID from the request body
	w.WriteHeader(http.StatusOK)
	var roomID models.Equipments
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&roomID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Get equipment for the room
	EquipmentsList, err := db.GetEquipmentForRoom(roomID.EquipID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Encode the equipment list into JSON and write it to the response
	json.NewEncoder(w).Encode(EquipmentsList)
	// w.Write([]byte("Got equipment for the room"))

}

func GetEquipmentTypesHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	equipmentTypes, err := db.GetEquipmentTypes()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(equipmentTypes)
}

func GetEquipmentHandler(w http.ResponseWriter, r *http.Request) {

	// Implement logic to retrieve bookings from the database
	ConferenceRooms, err := db.GetAllEquipments()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(ConferenceRooms)
}

func GetEquipmentsDefinitionsHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "GetEquipmentsDefinitions")
}

func AddNewEquipmentHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	// Parse JSON from the request body
	var newEquipment models.NewEquipments
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&newEquipment)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Add the new equipment to the database
	err = db.AddNewEquipment(newEquipment)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusCreated)
	// w.Write([]byte("New equipment added successfully"))
	w.Write([]byte("Nowy sprzęt dodany pomyślnie"))
}

func RemoveEquipmentsDefinitionsHandler(w http.ResponseWriter, r *http.Request) {
	// Parse JSON from the request body

	var EquipID models.Equipments
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&EquipID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Delete the equipment from the database
	err = db.DeleteEquipment(EquipID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	// w.Write([]byte("Equipment deleted successfully"))
	w.Write([]byte("Sprzęt usunięty pomyślnie"))
}

func UpdateEquipmentsDefinitionsHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	// Parse JSON from the request body
	var updatedEquipment models.Equipments
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&updatedEquipment)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Add the new equipment to the database
	err = db.UpdateEquipment(updatedEquipment)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusCreated) // Update the equipment in the database
	err = db.UpdateEquipment(updatedEquipment)

	// w.Write([]byte("Equipment updated successfully"))
	w.Write([]byte("Sprzęt zaktualizowany pomyślnie"))
}
