package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"srsgoapi/db"
	"srsgoapi/models"
)

// AddNewRoomHandler creates a new room in the database.
func AddNewRoomHandler(w http.ResponseWriter, r *http.Request) {
	// Check if the request is a preflight request (OPTIONS method)
	if r.Method == http.MethodOptions {
		w.WriteHeader(http.StatusOK)
		return
	}

	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// Parse JSON from the request body
	var newRoom models.ConferenceRoom
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&newRoom)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Create the room in the database
	err = db.AddNewRoom(newRoom)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusCreated)
	// w.Write([]byte(fmt.Sprintf("Room created successfully, ID: %d", newRoom.RoomID)))
	w.Write([]byte(fmt.Sprintf("Sala dodana pomyślnie, ID: %d", newRoom.RoomID)))
}

// UpdateRoomHandler updates a room in the database.
func UpdateRoomHandler(w http.ResponseWriter, r *http.Request) {
	// Check if the request is a preflight request (OPTIONS method)
	if r.Method == http.MethodOptions {
		w.WriteHeader(http.StatusOK)
		return
	}

	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	// Parse JSON from the request body
	var updatedRoom models.ConferenceRoom
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&updatedRoom)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Update the room in the database
	err = db.UpdateRoom(updatedRoom)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusOK)
	// w.Write([]byte("Room updated successfully"))
	w.Write([]byte("Sala zaktualizowana pomyślnie"))
}

// RemoveRoomHandler removes a room from the database.
func RemoveRoomHandler(w http.ResponseWriter, r *http.Request) {
	// Parse JSON from the request body
	var removeRoom models.ConferenceRoom
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&removeRoom)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer r.Body.Close()

	// Remove the room from the database
	err = db.RemoveRoom(removeRoom)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Respond with a success message or handle as needed
	w.WriteHeader(http.StatusOK)
	// w.Write([]byte("Room removed successfully"))
	w.Write([]byte("Sala usunięta pomyślnie"))
}
