package main

import (
	"encoding/json"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

type Message struct {
	Username string    `json:"username"`
	Text     string    `json:"text"`
	Time     time.Time `json:"time"`
	Channel  string    `json:"channel"`
}

type User struct {
	Conn     *websocket.Conn
	Username string
	Channel  string
}

var users = make(map[*websocket.Conn]*User)
var channels = make(map[string][]*websocket.Conn)
var messages = make([]Message, 0)
var mutex sync.Mutex
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func websocketHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println("Upgrade error:", err)
		return
	}
	defer conn.Close()

	username := r.URL.Query().Get("username")
	channel := r.URL.Query().Get("channel")

	if username == "" || channel == "" {
		log.Println("Username and channel are required")
		conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, "Username and channel are required"))
		return
	}

	user := &User{Conn: conn, Username: username, Channel: channel}

	mutex.Lock()
	users[conn] = user
	channels[channel] = append(channels[channel], conn)
	broadcastUserList(channel)
	mutex.Unlock()

	defer func() {
		mutex.Lock()
		delete(users, conn)
		channels[channel] = removeConn(channels[channel], conn)
		broadcastUserList(channel)
		mutex.Unlock()
	}()

	for {
		_, messageBytes, err := conn.ReadMessage()
		if err != nil {
			log.Println("Read error:", err)
			break
		}

		var msg Message
		err = json.Unmarshal(messageBytes, &msg)
		if err != nil {
			log.Println("Unmarshal error:", err)
			continue
		}

		msg.Time = time.Now()
		msg.Channel = user.Channel

		mutex.Lock()
		messages = append(messages, msg)
		mutex.Unlock()

		messageBytes, err = json.Marshal(msg)
		if err != nil {
			log.Println("Marshal error:", err)
			continue
		}

		mutex.Lock()
		for _, userConn := range channels[user.Channel] {
			if err := userConn.WriteMessage(websocket.TextMessage, messageBytes); err != nil {
				log.Println("Write error:", err)
				break
			}
		}
		mutex.Unlock()
	}
}

func broadcastUserList(channel string) {
	userList := []string{}
	for _, conn := range channels[channel] {
		userList = append(userList, users[conn].Username)
	}

	messageBytes, err := json.Marshal(map[string]interface{}{
		"type":  "userList",
		"users": userList,
	})
	if err != nil {
		log.Println("Marshal error:", err)
		return
	}

	for _, userConn := range channels[channel] {
		if err := userConn.WriteMessage(websocket.TextMessage, messageBytes); err != nil {
			log.Println("Write error:", err)
		}
	}
}

func removeConn(conns []*websocket.Conn, conn *websocket.Conn) []*websocket.Conn {
	for i, c := range conns {
		if c == conn {
			return append(conns[:i], conns[i+1:]...)
		}
	}
	return conns
}

func main() {
	http.HandleFunc("/ws", websocketHandler)
	log.Fatal(http.ListenAndServe(":7074", nil))
}
