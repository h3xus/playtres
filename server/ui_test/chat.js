document.getElementById('connectBtn').addEventListener('click', connect);

let socket;
let username;
let channel;

function connect() {
    username = document.getElementById('username').value;
    channel = document.getElementById('channel').value;

    if (!username || !channel) {
        alert('Username and channel are required!');
        return;
    }

    const url = `ws://localhost:7074/ws?username=${username}&channel=${channel}`;
    socket = new WebSocket(url);

    socket.onopen = () => {
        console.log('Connected to WebSocket server');
        document.getElementById('chat').style.display = 'block';
    };

    socket.onmessage = (event) => {
        const msg = JSON.parse(event.data);
        if (msg.type === 'userList') {
            updateUserList(msg.users);
        } else {
            displayMessage(msg);
        }
    };

    socket.onclose = () => {
        console.log('Disconnected from WebSocket server');
        document.getElementById('chat').style.display = 'none';
    };

    socket.onerror = (error) => {
        console.error('WebSocket error:', error);
    };
}

document.getElementById('sendBtn').addEventListener('click', sendMessage);

function sendMessage() {
    const messageInput = document.getElementById('messageInput');
    const message = messageInput.value;
    if (!message) {
        return;
    }

    const msg = {
        username: username,
        text: message,
        channel: channel
    };

    socket.send(JSON.stringify(msg));
    messageInput.value = '';
}

function displayMessage(msg) {
    const messagesDiv = document.getElementById('messages');
    const messageElement = document.createElement('div');
    messageElement.innerHTML = `<strong>${msg.username}</strong> [${new Date(msg.time).toLocaleTimeString()}]: ${msg.text}`;
    messagesDiv.appendChild(messageElement);
    messagesDiv.scrollTop = messagesDiv.scrollHeight;
}

function updateUserList(users) {
    const userList = document.getElementById('userList');
    userList.innerHTML = '';
    users.forEach((user) => {
        const userElement = document.createElement('li');
        userElement.textContent = user;
        userList.appendChild(userElement);
    });
}
