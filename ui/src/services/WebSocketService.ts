export default class WebSocketService {
    private url: string;
    private username: string;
    private channel: string;
    private onMessageCallback: (msg: any) => void;
    private onUserListCallback: (users: string[]) => void;
    private socket: WebSocket | null = null;

    constructor(
        url: string,
        username: string,
        channel: string,
        onMessageCallback: (msg: any) => void,
        onUserListCallback: (users: string[]) => void
    ) {
        this.url = `${url}?username=${username}&channel=${channel}`;
        this.username = username;
        this.channel = channel;
        this.onMessageCallback = onMessageCallback;
        this.onUserListCallback = onUserListCallback;
    }

    connect(): void {
        this.socket = new WebSocket(this.url);

        this.socket.onopen = () => {
            console.log('Connected to WebSocket server');
        };

        this.socket.onmessage = (event: MessageEvent) => {
            const msg = JSON.parse(event.data);
            if (msg.type === 'userList') {
                this.onUserListCallback(msg.users);
            } else {
                this.onMessageCallback(msg);
            }
        };

        this.socket.onclose = (event: CloseEvent) => {
            console.log('Disconnected from WebSocket server', event);
            if (event.reason) {
                alert(`Disconnected: ${event.reason}`);
            }
        };

        this.socket.onerror = (error: Event) => {
            console.error('WebSocket error:', error);
        };
    }

    sendMessage(text: string): void {
        if (!this.socket || this.socket.readyState !== WebSocket.OPEN) {
            return;
        }

        const msg = {
            username: this.username,
            text: text,
            channel: this.channel
        };

        this.socket.send(JSON.stringify(msg));
    }

    disconnect(): void {
        if (this.socket) {
            this.socket.close();
        }
    }
}
