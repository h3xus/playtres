import global from './global.js';
import accordion from './accordion/index.js';
import checkbox from './checkbox/index.js';
import card from './card/index.js';
import deferred from './deferred/index.js';
import divider from './divider/index.js';
import fieldset from './fieldset/index.js';

import inputtext from './inputtext/index.js';
import panel from './panel/index.js';
import scrollpanel from './scrollpanel/index.js';
import splitter from './splitter/index.js';
import stepper from './stepper/index.js';
import tabview from './tabview/index.js';
import toolbar from './toolbar/index.js';
import selectbutton from './selectbutton/index.js';

export default {
    global,
    accordion,
    checkbox,
    card,
    deferred,

    inputtext,
    divider,
    fieldset,
    panel,
    scrollpanel,
    splitter,
    stepper,
    tabview,
    toolbar,
    selectbutton
}
