import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
// import { useUser } from '@/shared/stores/user';

const routes: Array<RouteRecordRaw> = [
    { path: "/", component: () => import("@views/Home.vue") },
    { path: "/main", component: () => import("@views/Main.vue"), meta: { requiresAuth: false } },
    // { path: "/home", component: () => import("@views/Home.vue"), meta: { requiresAuth: true } },
    // { path: "/edit", component: () => import("@views/Edit.vue"), meta: { requiresAuth: true } },
    // { path: "/list", component: () => import("@views/List.vue"), meta: { requiresAuth: true } },
    // { path: "/booking", component: () => import("@views/Events.vue"), meta: { requiresAuth: true } },
    // { path: "/cal", component: () => import("@views/Cal.vue"), meta: { requiresAuth: true } },
    // { path: '/cal/:bookingId', component: () => import("@views/CalDay.vue"), meta: { requiresAuth: true } },
    // { path: "/locs", component: () => import("@views/Locals.vue"), meta: { requiresAuth: true } },
    // { path: "/login", component: () => import("@views/Login.vue") },
    // { path: "/display", component: () => import("@views/Display.vue") },
    // { path: "/:pathMatch(.*)*", component: () => import("@views/NotFound.vue") }, // 404 handling
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

// router.beforeEach((to, from, next) => {
//     const store = useUser();
//     store.resetSelectedData();
//     store.setSelectedLoc(null);
//     store.setSelectedRoom(null);
//     if (to.meta.requiresAuth && !store.isLoggedIn) {
//         next({ path: '/login' });
//     } else {
//         next();
//     }
// });

export default router