import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// import { fileURLToPath, URL } from "node:url"
import path from 'path';

import { templateCompilerOptions } from '@tresjs/core'

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  const apiURL = command === 'serve' ? 'http://localhost:3000/api' : 'https://prod.api.example.com/api';

  return {
    server: {
      host: true,
      watch: {
        usePolling: true
      },
    },
    build: {
      outDir: 'dist',
      sourcemap: true, // Changed from false to true
      rollupOptions: {
        output: {
          manualChunks: {
            vendor: ['vue', 'vue-router', 'axios']
          }
        }
      }
    },
    define: {
      __API_URL__: JSON.stringify(apiURL),
    },
    optimizeDeps: {
      include: [
        'vue',
        'vue-router',
        'vue-i18n',
        'axios' // Added 'axios' to the include array
      ],
    },
    plugins: [vue({
      // Other config
      ...templateCompilerOptions
    }),],
    resolve: {
      alias: {
        '@': path.resolve(__dirname,'./src'),
        '@services': path.resolve(__dirname,'./src/services'),
        '@views': path.resolve(__dirname,'./src/views'),
        '@components': path.resolve(__dirname,'./src/components'),
        '@cmp': path.resolve(__dirname,'./src/shared/cmp'),
        '@stores': path.resolve(__dirname,'./src/shared/stores'),
        '@use': path.resolve(__dirname,'./src/shared/use'),
        '@utils': path.resolve(__dirname,'./src/shared/utils'), // Added '@utils' alias
      },
    },
  }
})

