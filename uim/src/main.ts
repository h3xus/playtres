import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import PrimeVue from 'primevue/config'
import Aura from '@primevue/themes/aura'

//cheryy pick components
import Accordion from 'primevue/accordion'
import Button from 'primevue/button'
import DatePicker from 'primevue/datepicker'
import Card from 'primevue/card'
import Chip from 'primevue/chip'
import Column from 'primevue/column'
import ColumnGroup from 'primevue/columngroup'  // optional

import Listbox from 'primevue/listbox'

import ConfirmPopup from 'primevue/confirmpopup'

import Checkbox from "primevue/checkbox"
import DataTable from 'primevue/datatable'
import Dialog from 'primevue/dialog'

import InputText from 'primevue/inputtext'
import InputNumber from 'primevue/inputnumber'

import TabPanel from 'primevue/tabpanel'
import Tooltip from 'primevue/tooltip'
import Panel from "primevue/panel"
import Row from 'primevue/row'                   // optional
import Steps from 'primevue/steps'
import Message from 'primevue/message'
import SelectButton from 'primevue/selectbutton'
import ToggleButton from 'primevue/togglebutton'
import Fieldset from 'primevue/fieldset'
import ScrollPanel from 'primevue/scrollpanel'
import PickList from 'primevue/picklist'
import Toast from 'primevue/toast'
import ToastService from 'primevue/toastservice'
// import { createPinia } from 'pinia'
// import router from './router'

const app = createApp(App)
const components = {
    Accordion, Panel, InputNumber, Button, SelectButton, ToggleButton, Checkbox,
    ConfirmPopup, Toast, TabPanel, DatePicker, Card, DataTable, Dialog, Column, ColumnGroup, Row, ScrollPanel,
    InputText, Steps, Message, Fieldset, Chip, PickList, Listbox
}

// app.use(PrimeVue, { unstyled: false, pt: Lara, locale: pl.pl })
Object.entries(components).forEach(([name, component]) => {
    app.component(name, component)
})

// app.use(createPinia())
app.directive('tooltip', Tooltip)

// app.use(router)
// app.use(plugin, defaultConfig(config))
app.use(ToastService)

// app.use(i18n)

app.mount("#app")

app.use(PrimeVue, {
    theme: {
        preset: Aura
    }
})